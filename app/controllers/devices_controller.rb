class DevicesController < ApplicationController
  def index
    if params[:name].present?
      devices = case params[:provider]
                  when 'gsmarena'
                    search_provider_gsmarena params[:name]
                  else
                    search_provider_gsmarena params[:name]
                end

      render json: devices
    else
      render json: []
    end
  end

  def show
    info = case params[:provider]
                when 'gsmarena'
                  info_provider_gsmarena params[:id]
                else
                  info_provider_gsmarena params[:id]
              end

    render info
  end

  private

  def search_provider_gsmarena name
    url = URI.encode "http://www.gsmarena.com/results.php3?sQuickSearch=yes&sName=#{name}"

    doc = Nokogiri::HTML(open(url))

    devices = []

    doc.css('div.makers ul li').each do |link|
      devices << {
          name: link.content,
          href: link.css('a').attr('href').content.split('.').first,
          src: link.css('a img').attr('src').content
      }

    end

    devices
  end

  def info_provider_gsmarena id
    doc = Nokogiri::HTML(open("http://www.gsmarena.com/#{id}.php"))

    info = doc.css('[id="specs-list"] table')

    {
        json: {
            name: doc.css('h1.specs-phone-name-title').text,
            image: doc.css('div.specs-photo-main').css('img').attr('src').content,
            network: info[0].css('tr')[0].css('td')[1].text,
            memory: info[5].css('tr')[0].css('td')[1].text,
            battery: info[10].css('tr')[0].css('td')[1].text,
            display_type: info[3].css('tr')[0].css('td')[1].text,
            display_size: info[3].css('tr')[1].css('td')[1].text,
            display_resolution: info[3].css('tr')[2].css('td')[1].text
        },
        status: 200
    }

  rescue OpenURI::HTTPError => error
    {
        json: {},
        status: 404
    }
  end

end
