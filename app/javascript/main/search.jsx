// Run this example by adding <%= javascript_pack_tag 'hello_react' %> to the head of your layout file,
// like app/views/layouts/application.html.erb. All it does is render <div>Searcher React</div> at the bottom
// of the page.

import React from 'react'
import './index.sass'
import empty from 'is-empty'
import 'bootstrap/dist/css/bootstrap.css'
import 'font-awesome/css/font-awesome.css'
import axios from 'axios'
import {
  Link
} from 'react-router-dom'

export default class Searcher extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      devices: [],
      loading: false
    }

    this.getDevices = this.getDevices.bind(this);
  }

  getDevices(event) {
    this.setState({
      loading: true
    })

    let CancelToken = axios.CancelToken
    let source = CancelToken.source()

    if (!empty(this.previos_request)) {
      this.previos_request.cancel('Operation canceled by the user.')
    }

    axios.get(`/devices?name=${event.target.value}&provider=gsmarena`, {
      cancelToken: source.token
    })
      .then(function (response) {
        console.log(response)
        this.setState({
          devices: response.data,
          loading: false
        })
      }.bind(this))
      .catch(function (error) {
        console.log(error)
        this.setState({
          loading: false
        })
      }.bind(this))

    this.previos_request = source
  }

  render() {
    return(
      <div>
        <div className="row">
          <div className="col">
            <input className="form-control" onChange={this.getDevices} type="text" placeholder="Search"/>
          </div>
        </div>

        <div className="row justify-content-center">
          <div className="col-1 mt-4">
            { this.state.loading &&
            <div>
              <i className="fa fa-spinner fa-pulse fa-2x fa-fw"/>
            </div>

            }
          </div>
        </div>

        <br/>

        <div className="row">
          <div className="col">
            <ul className="list-group">

              { this.state.devices.map(( (device, index) => {
                return(
                  <Link key={index} to={`pages/${device.href}?provider=gsmarena`} className="list-group-item list-group-item-action">
                    <img src={device.src} width="80" height="100"/>
                    <div className="ml-3">{device.name}</div>
                  </Link>
                )
              }))}

            </ul>
          </div>

        </div>

      </div>
    )
  }
}
