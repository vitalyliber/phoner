import Search from './search'
import Show from './show'
import ReactDOM from 'react-dom'
import React from 'react'
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'


ReactDOM.render(
  <Router>
    <div className="container">

      <div className="row">
        <div className="col mt-2">
          <Link to="/">Phone searcher!</Link>
        </div>
      </div>

      <hr/>

      <Route exact path="/" component={Search}/>
      <Route exact path="/pages/:id" component={Show}/>

    </div>
  </Router>,
  document.body.appendChild(document.createElement('div')),
)