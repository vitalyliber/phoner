import React from 'react'
import './index.sass'
import empty from 'is-empty'
import 'bootstrap/dist/css/bootstrap.css'
import axios from 'axios'

export default class Show extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      device: {},
      error: null
    }

    this.getDeviceInfo = this.getDeviceInfo.bind(this);
  }

  componentDidMount() {
    this.getDeviceInfo()
  }

  getDeviceInfo() {
    if (this.props.match.params.id.length > 0) {
      axios.get(`/devices/${this.props.match.params.id}?provider=gsmarena`)
        .then(function (response) {
          console.log(response)
          this.setState({
            device: response.data
          })
        }.bind(this))
        .catch(function (error) {
          console.log(error.response.status)
          let error_message
          if (error.response.status === 404) {
            error_message = 'The page cannot be found 😙'
          } else {
            error_message = 'Something went wrong 👾'
          }
          this.setState({
            error: error_message
          })
        }.bind(this))
    } else {
      this.setState({
        device: {}
      })
    }

  }

  render() {
    return(
      <div className="row">
        <div className="col">
          {!empty(this.state.device) ? (
            <div>
              <h1>{this.state.device.name}</h1>
              <img className="card-img-top"
                   src={this.state.device.image}
                   alt={this.state.device.name}/>
              <p className="font-weight-bold mt-2">Display type:
                <span className="font-weight-normal ml-1">
                  {this.state.device.display_type}
                </span>
              </p>
              <p className="font-weight-bold">Display size:
                <span className="font-weight-normal ml-1">
                  {this.state.device.display_size}
                </span>
              </p>
              <p className="font-weight-bold">Display resolution:
                <span className="font-weight-normal ml-1">
                  {this.state.device.display_resolution}
                </span>
              </p>
              <p className="font-weight-bold">Battery:
                <span className="font-weight-normal ml-1">
                  {this.state.device.battery}
                </span>
              </p>
              <p className="font-weight-bold">Memory:
                <span className="font-weight-normal ml-1">
                  {this.state.device.memory}
                </span>
              </p>
              <p className="font-weight-bold">Network:
                <span className="font-weight-normal ml-1">
                  {this.state.device.network}
                </span>
              </p>
            </div>
          ) : (
            <div>
              { empty(this.state.error) ? (
                'Loading...'
              ) : (
                this.state.error
              )}
            </div>
          )
          }
        </div>
      </div>
    )
  }
}
